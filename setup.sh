#!/bin/bash
#-Metadata----------------------------------------------------#
#  Filename: setup.sh                                         #
#-Info--------------------------------------------------------#
#  Automating Ubuntu/Debian/RHEL Secure Setup Configuration.  #
#-Author(s)---------------------------------------------------#
#  Cameron Poe ~ https://blog.zay.li/                         #
#-Operating System--------------------------------------------#
# Tested on: CentOS 7.4 x64 Ubuntu 16.04.4 x64 Debian 9.3 x64 #
#-Licence-----------------------------------------------------#
#  MIT License ~ http://opensource.org/licenses/MIT           #
#-Notes-------------------------------------------------------#
#  Run as root                                                #
#                             ---                             #                         #
#  Command line arguments:                                    #
#    username  = New Admin User to be created                 #
#    port      = New SSH Port                                 #
#                                                             #
#                             ---                             #
#             ** This script is meant for _ME_. **            #
#         ** EDIT this to meet _YOUR_ requirements! **        #
#-------------------------------------------------------------#


##### (Optional) Enable debug mode?
#set -x

##### (Cosmetic) Colour output
RED="\033[01;31m"      # Issues/Errors
GREEN="\033[01;32m"    # Success
YELLOW="\033[01;33m"   # Warnings/Information
BOLD="\033[01;01m"     # Highlight
RESET="\033[00m"       # Normal

if [[ $EUID -ne 0 ]]; then
   echo -e ' '${RED}'[!]'${RESET}" This script must be ${RED}run as root${RESET}. Quitting..." 1>&2
   exit 1
fi

if [ -z "$1" ]; then
    echo -e ' '${RED}'[!]'${RESET}" ${0} ${RED}${BOLD}<newuser> <sshport>${RESET}" 1>&2
    exit 1
fi

if [ ! -f "$HOME/.ssh/authorized_keys" ]; then
    echo -e ' '${RED}'[!]'${RESET}" ${RED}No SSH Public Key present for current user.${RESET}" 1>&2
    echo -e ' '${RED}'[!]'${RESET}" ${RED}To generate one -> ssh-keygen -t rsa -b 4096 -C <your_email@example.com>${RESET}" 1>&2
    exit 1
fi

if [ -f /etc/redhat-release ]; then
    yum install -y wget
fi

### Check Internet access ###
echo -e "\n ${GREEN}[+]${RESET} Checking ${GREEN}Internet access${RESET}"
wget -q --spider http://google.com
if [ $? -eq 0 ]; then
    echo -e "\n ${GREEN}[+]${RESET} ${GREEN}Connected!${RESET}"
else
    echo -e ' '${RED}'[!]'${RESET}" ${RED}No Internet access! Quitting...${RESET}" 1>&2
    exit 1
fi

### Red Hat Enterprise Linux ###
if [ -f /etc/redhat-release ]; then
  DIST="$(cat /etc/redhat-release | cut -d ' ' -f1)"
  echo -e "\n ${GREEN}[+]${RESET} Checking Distribution...${GREEN}${DIST}${RESET}"
  yum update -y
  PM="yum"
  echo -e "\n ${GREEN}[+]${RESET} Installing epel and firewalld"
  yum install -y epel-release
  yum install -y firewalld
  echo -e "\n ${GREEN}[+]${RESET} Starting and Enabling firewalld via systemd"
  systemctl start firewalld
  systemctl enable firewalld
  firewall-cmd --zone=public --permanent --add-service=ssh
  ### Add if you use Portmapper ###
  #firewall-cmd --permanent --add-service=nfs
  #firewall-cmd --permanent --add-service=mountd
  #firewall-cmd --permanent --add-service=rpc-bind
  firewall-cmd --reload
### Debian/Ubuntu ###
elif [ -f /etc/debian_version ]; then
  DIST="$(lsb_release -i | cut -f 2-)"
  echo -e "\n ${GREEN}[+]${RESET} Checking Distribution...${GREEN}${DIST}${RESET}"
  apt-get update
  PM="apt"
  echo -e "\n ${GREEN}[+]${RESET} Installing UFW"
  apt install -y -qq ufw
  ### Be sure to use the 'ufw allow' command open a service. You can use the port or service name. ###
  echo -e "\n ${GREEN}[+]${RESET} Allowing SSH service with UFW"
  systemctl restart ufw
  ufw allow $2/tcp
  ufw enable
else
  echo -e ' '${RED}'[!]'${RESET}" ${RED}Distribution Not Supported! Quitting...${RESET}" 1>&2
  exit 1
fi

USER=$1
SSH=$2

### Setup user ###
echo -e "\n ${GREEN}[+]${RESET} Creating User..."
adduser $USER
echo -e "\n ${GREEN}[+]${RESET} Adding user to sudoer"
if [ -f /etc/redhat-release ]; then
   passwd $USER
   usermod -aG wheel $USER
fi
if [ -f /etc/debian_version ]; then
   usermod -aG sudo $USER
fi


echo -e "\n ${GREEN}[+]${RESET} Moving Current SSH Key to $USER"
mkdir /home/$USER/.ssh
cat /root/.ssh/authorized_keys >> /home/$USER/.ssh/authorized_keys
chown -R $USER:$USER /home/$USER/.ssh/

echo -e "\n ${GREEN}[+]${RESET} Installing Fail2Ban"
$PM install -y fail2ban
cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.conf.backup
systemctl restart fail2ban.service

### SSH Config Change ###
echo -e "\n ${GREEN}[+]${RESET} Changing port to ${RED}$SSH${RESET}. Disabling root login and password authentication"

if [ -f /etc/redhat-release ]; then
   sed -i "s/^#Port .*/Port $SSH/g" /etc/ssh/sshd_config
   sed -i 's/#PermitRootLogin yes/PermitRootLogin no/g' /etc/ssh/sshd_config
   semanage port -a -t ssh_port_t -p tcp $SSH
   systemctl restart sshd.service
elif [ ${DIST} == "Ubuntu" ]; then
   sed -i "s/^Port .*/Port $SSH/g" /etc/ssh/sshd_config
   sed -i 's/PermitRootLogin yes/PermitRootLogin no/g' /etc/ssh/sshd_config
   sed -i 's/#PasswordAuthentication yes/PasswordAuthentication no/g' /etc/ssh/sshd_config
   systemctl restart ssh.service
elif [ ${DIST} == "Debian" ]; then
   sed -i "s/^#Port .*/Port $SSH/g" /etc/ssh/sshd_config
   sed -i 's/PermitRootLogin yes/PermitRootLogin no/g' /etc/ssh/sshd_config
   sed -i 's/#PasswordAuthentication yes/PasswordAuthentication no/g' /etc/ssh/sshd_config
   systemctl restart ssh.service
fi

echo -e "\n ${GREEN}[+]${RESET} Adding important iptables rules"

echo -e "\n ${YELLOW}[!]${RESET}${RED} 1: Dropping invalid packets${RESET}"
/sbin/iptables -t mangle -A PREROUTING -m conntrack --ctstate INVALID -j DROP
echo -e "\n ${YELLOW}[!]${RESET}${RED} 2: Drop TCP packets that are new and are not SYN${RESET}"
/sbin/iptables -t mangle -A PREROUTING -p tcp ! --syn -m conntrack --ctstate NEW -j DROP

echo -e "\n ${YELLOW}[!]${RESET}${RED} 3: Drop SYN packets with suspicious MSS value${RESET}"
/sbin/iptables -t mangle -A PREROUTING -p tcp -m conntrack --ctstate NEW -m tcpmss ! --mss 536:65535 -j DROP

echo -e "\n ${YELLOW}[!]${RESET}${RED} 4: Block packets with bogus TCP flags${RESET}"
/sbin/iptables -t mangle -A PREROUTING -p tcp --tcp-flags FIN,SYN,RST,PSH,ACK,URG NONE -j DROP
/sbin/iptables -t mangle -A PREROUTING -p tcp --tcp-flags FIN,SYN FIN,SYN -j DROP
/sbin/iptables -t mangle -A PREROUTING -p tcp --tcp-flags SYN,RST SYN,RST -j DROP
/sbin/iptables -t mangle -A PREROUTING -p tcp --tcp-flags FIN,RST FIN,RST -j DROP
/sbin/iptables -t mangle -A PREROUTING -p tcp --tcp-flags FIN,ACK FIN -j DROP
/sbin/iptables -t mangle -A PREROUTING -p tcp --tcp-flags ACK,URG URG -j DROP
/sbin/iptables -t mangle -A PREROUTING -p tcp --tcp-flags ACK,FIN FIN -j DROP
/sbin/iptables -t mangle -A PREROUTING -p tcp --tcp-flags ACK,PSH PSH -j DROP
/sbin/iptables -t mangle -A PREROUTING -p tcp --tcp-flags ALL ALL -j DROP
/sbin/iptables -t mangle -A PREROUTING -p tcp --tcp-flags ALL NONE -j DROP
/sbin/iptables -t mangle -A PREROUTING -p tcp --tcp-flags ALL FIN,PSH,URG -j DROP
/sbin/iptables -t mangle -A PREROUTING -p tcp --tcp-flags ALL SYN,FIN,PSH,URG -j DROP
/sbin/iptables -t mangle -A PREROUTING -p tcp --tcp-flags ALL SYN,RST,ACK,FIN,URG -j DROP

echo -e "\n ${YELLOW}[!]${RESET}${RED} 5: Block spoofed packets${RESET}"
/sbin/iptables -t mangle -A PREROUTING -s 224.0.0.0/3 -j DROP
/sbin/iptables -t mangle -A PREROUTING -s 169.254.0.0/16 -j DROP
/sbin/iptables -t mangle -A PREROUTING -s 172.16.0.0/12 -j DROP
/sbin/iptables -t mangle -A PREROUTING -s 192.0.2.0/24 -j DROP
/sbin/iptables -t mangle -A PREROUTING -s 192.168.0.0/16 -j DROP
/sbin/iptables -t mangle -A PREROUTING -s 10.0.0.0/8 -j DROP
/sbin/iptables -t mangle -A PREROUTING -s 0.0.0.0/8 -j DROP
/sbin/iptables -t mangle -A PREROUTING -s 240.0.0.0/5 -j DROP
/sbin/iptables -t mangle -A PREROUTING -s 127.0.0.0/8 ! -i lo -j DROP

echo -e "\n ${YELLOW}[!]${RESET}${RED} 6: Dropping ICMP (ping will not work)${RESET}"
/sbin/iptables -t mangle -A PREROUTING -p icmp -j DROP

echo -e "\n ${YELLOW}[!]${RESET}${RED} 7: Dropping fragments in all chains${RESET}"
/sbin/iptables -t mangle -A PREROUTING -f -j DROP

echo -e "\n ${YELLOW}[!]${RESET}${RED} 8: Limiting connections per source IP${RESET}"
/sbin/iptables -A INPUT -p tcp -m connlimit --connlimit-above 111 -j REJECT --reject-with tcp-reset

echo -e "\n ${YELLOW}[!]${RESET}${RED} 9: Limiting RST packets${RESET}"
/sbin/iptables -A INPUT -p tcp --tcp-flags RST RST -m limit --limit 2/s --limit-burst 2 -j ACCEPT
/sbin/iptables -A INPUT -p tcp --tcp-flags RST RST -j DROP

echo -e "\n ${YELLOW}[!]${RESET}${RED} 10: Limiting new TCP connections per second per source IP${RESET}"
/sbin/iptables -A INPUT -p tcp -m conntrack --ctstate NEW -m limit --limit 60/s --limit-burst 20 -j ACCEPT
/sbin/iptables -A INPUT -p tcp -m conntrack --ctstate NEW -j DROP


echo -e "\n ${YELLOW}[!]${RESET}${RED} 11: Use SYNPROXY on all ports (disables connection limiting rule)${RESET}"
echo -e "\n ${YELLOW}[!]${RESET}${RED} Hidden - unlock content above in 'Mitigating SYN Floods With SYNPROXY' section${RESET}"
echo -e "\n ${YELLOW}[!]${RESET}${RED} SSH brute-force protection ${RESET}"
/sbin/iptables -A INPUT -p tcp --dport ssh -m conntrack --ctstate NEW -m recent --set
/sbin/iptables -A INPUT -p tcp --dport ssh -m conntrack --ctstate NEW -m recent --update --seconds 60 --hitcount 10 -j DROP

echo -e "\n ${YELLOW}[!]${RESET}${RED} Protection against port scanning${RESET}"
/sbin/iptables -N port-scanning
/sbin/iptables -A port-scanning -p tcp --tcp-flags SYN,ACK,FIN,RST RST -m limit --limit 1/s --limit-burst 2 -j RETURN
/sbin/iptables -A port-scanning -j DROP

ipv4=$(ip route get 1 | awk '{print $NF;exit}')

echo -e "\n ${YELLOW}[!]${RESET} ssh $1@$ipv4 -p $2"

echo -e '\n'${BLUE}'[*]'${RESET}' '${BOLD}'Done!'${RESET}'\n\a'
exit 0
