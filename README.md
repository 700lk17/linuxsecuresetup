# Linux Secure Setup

Automating Ubuntu/Debian/RHEL Secure Setup Configuration.

Tested on CentOS 7.4 x64 | Ubuntu 16.04.4 x64 | Debian 9.3 x64

### Features
Automatic SSH Secure configuration setup<br />
Iptables Dropping invalid packets, TCP that are new and are not SYN, SYN with suspicious MSS value, bogus TCP flags, spoofed packets, ICMP, fragments in all chains, Limiting connections per source IP, Limiting RST packets, Limiting new TCP connections per second per source IP.
<br />
Uses SYNPROXY on all ports (disables connection limiting rule)
<br />
Script works with no specification of current OS

### Motivation
Had multiple ESXi VM's and wanted to easly execute one script to automatically configure and setup my standard security practices on both Debian and RHEL based distributions without having to specify the OS at all.

### Installing
```
$ git clone https://github.com/cameronpoe/linuxsecuresetup
$ cd linuxsecuresetup; chmod +x setup.sh
```


### Usage
This script assumes you already have generated a ssh key and its already present within ```.ssh/authorized_keys```.<br />
To generate a key ```ssh-keygen -t rsa -b 4096```
``` 
$ ./setup.sh <newuser> <sshport>
Example: ./setup.sh zay 1337
```

## Contributing
Pull request are welcome

## Author

* **Cameron Poe** - *zay* - [Blog](https://blog.zay.li/)



## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details
